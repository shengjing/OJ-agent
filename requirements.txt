
fastapi>=0.109.2
uvicorn>=0.27.1
starlette>=0.27.0

camel-ai>=0.2.22
openai>=1.0.0

python-dotenv>=1.0.0
requests>=2.31.0
pydantic>=2.6.1
typing-extensions>=4.11.0
loguru>=0.7.2
asyncio>=3.4.3

pytest>=8.0.0
pytest-asyncio>=0.23.5
pytest-cov>=4.1.0

black>=24.1.1
isort>=5.13.2
pillow

from camel.types import ModelPlatformType

from camel.models import ModelFactory
from camel.types.enums import ModelPlatformType

import openai
api_key = "e6a81620-5dea-41fa-901b-4582258451db"
base_url = "https://api-inference.modelscope.cn/v1/"

model = ModelFactory.create(
    model_platform=ModelPlatformType.OPENAI_COMPATIBLE_MODEL,  # ModelScope 兼容 OpenAI API
    model_type="deepseek-ai/DeepSeek-V3",  # ModelScope 的 Model ID
    api_key=api_key,
    url=base_url
)

response = model.run([
    {"role": "system", "content": "You are a helpful assistant."},
    {"role": "user", "content": "你好"}
])

print(response.choices[0].message.content)
